package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"github.com/astaxie/beego"
)

// Model Struct
type User struct {
	Id   int  `orm:"auto"`
	//ID 			int32 			`json:"id" db:"id"`
	//Name string `orm:"size(100)"`
	Name 		string 			`json:"Name" db:"Name"`
}

//func checkErr(err error) {
//	if err != nil {
//		panic(err)
//	}
//}
//新增用户
func Create(name string)  (bool,int64){
	var (
		n int64
		err error
	)
	o := orm.NewOrm()
	o.Using("test")
	newuser:=new(User)
	//赋值给模型
	//newuser.Id = id
	newuser.Name = fmt.Sprintf("%s",string(name))
	fmt.Println("Insert1111111111 >>>", newuser.Name)
	if newuser.Name == "" {
		return false, n
	}
	//新增数据
	if n,err=o.Insert(newuser);err!=nil{
		fmt.Println("Insert >>>", n, err)
		return false, n
	}

	return true, n
}

//根据id查询
func QueryById(id int) (User, bool){

	o := orm.NewOrm()
	u := User{Id: id}

	err := o.Read(&u)

	if err == orm.ErrNoRows {
		fmt.Println("查询不到")
		return u, false
	} else if err == orm.ErrMissPK {
		fmt.Println("找不到主键")
		return u, false
	} else {
		fmt.Println(u.Id, u.Name)
		return u, true
	}
}

func Del(id int) (bool){
	fmt.Printf("id=%d", id)
	o := orm.NewOrm()
	if num, err := o.Delete(&User{Id: id}); err == nil {
		fmt.Println(num)
		return false
	} else {
		return true
	}
}

//根据用户数据列表
func DataList() (users []User) {

	o := orm.NewOrm()
	qs := o.QueryTable("user")

	var us []User
	cnt, err :=  qs.Filter("id__gt", 0).OrderBy("-id").Limit(10, 0).All(&us)
	if err == nil {
		fmt.Printf("count>>>>>>>>>>>>>>>", cnt)
	}
	return us
}

func UpdateById(id int,opt User)bool{
	//o := orm.NewOrm()
	//_, err := o.QueryTable(
	//	table).Filter(
	//	"Id", id).Update(
	//	filed)
	//if err == nil{
	//	return true
	//}
	//return false
	fmt.Println("进来了...");

	o := orm.NewOrm()
	user := User{Id: id}

	fmt.Println("进来了...333", id);
	user.Name =  opt.Name
	if num, err := o.Update(&user); err != nil {
		fmt.Println("x写入失败",err)
	}else{
		fmt.Println(num)
	}


	fmt.Println("进来了...6666");
	return true
}

func init(){
	//dbhost := beego.AppConfig.String("dbhost")
	//dbport := beego.AppConfig.String("dbport")
	//dbuser := beego.AppConfig.String("dbuser")
	//dbpassword := beego.AppConfig.String("dbpassword")
	//db := beego.AppConfig.String("db")
	dbhost := "127.0.0.1"
	dbport := "3306"
	dbuser := "root"
	dbpassword := "root"
	db := "test"
	fmt.Println("db ===========",db)

	//注册mysql Driver
	orm.RegisterDriver("mysql", orm.DRMySQL)
	//构造conn连接
	//用户名:密码@tcp(url地址)/数据库
	conn := dbuser + ":" + dbpassword + "@tcp(" + dbhost + ":" + dbport + ")/" + db + "?charset=utf8"
	//注册数据库连接
	orm.RegisterDataBase("test", "mysql", conn)

	orm.RegisterModel(new(User))
	orm.RegisterModel(new(Project))

	fmt.Printf("数据库连接成功！%s\n", conn)
}
func main() {
	o := orm.NewOrm()
	o.Using("test") // 默认使用 default，你可以指定为其他数据库
	beego.Run()
}