package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

// Model Struct
type Project struct {
	Id   int  `orm:"auto" json:"Id" db:"id"`
	Name string `json:"Name" db:"name"`
}

//func checkErr(err error) {
//	if err != nil {
//		panic(err)
//	}
//}
//新增用户
func CreateProject(name string)  (bool,int64){
	var (
		n int64
		err error
	)

	o := orm.NewOrm()
	o.Using("test")
	newuser:=new(Project)
	//赋值给模型
	//newuser.Id = id
	newuser.Name = fmt.Sprintf("%s",string(name))
	fmt.Println("Insert1111111111 >>>", newuser.Name)
	if newuser.Name == "" {
		return false, n
	}
	//新增数据
	if n,err=o.Insert(newuser);err!=nil{
		fmt.Println("Insert >>>", n, err)
		return false, n
	}

	return true, n
}

//根据id查询
func QueryByIdProject(id int) (Project, bool){

	o := orm.NewOrm()
	u := Project{Id: id}

	err := o.Read(&u)

	if err == orm.ErrNoRows {
		fmt.Println("查询不到")
		return u, false
	} else if err == orm.ErrMissPK {
		fmt.Println("找不到主键")
		return u, false
	} else {
		fmt.Println(u.Id, u.Name)
		return u, true
	}
}

func DelProject(id int) (bool){
	fmt.Printf("id=%d", id)
	o := orm.NewOrm()
	if num, err := o.Delete(&User{Id: id}); err == nil {
		fmt.Println(num)
		return false
	} else {
		return true
	}
}

//根据用户数据列表
func DataListProject() (project []Project) {

	o := orm.NewOrm()
	qs := o.QueryTable("project")

	var us []Project
	cnt, err :=  qs.Filter("id__gt", 0).OrderBy("-id").Limit(10, 0).All(&us)
	if err == nil {
		fmt.Printf("count>>>>>>>>>>>>>>>", cnt)
	}
	return us
}

func UpdateByIdProject(id int,opt Project)bool{
	fmt.Println("进来了...");

	o := orm.NewOrm()
	project := Project{Id: id}

	fmt.Println("进来了...333", id);
	project.Name =  opt.Name
	if num, err := o.Update(&project); err != nil {
		fmt.Println("x写入失败",err)
	}else{
		fmt.Println(num)
	}


	fmt.Println("进来了...6666");
	return true
}


