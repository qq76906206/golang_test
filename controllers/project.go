package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"myproject/models"
	"fmt"
	"strconv"
)

type ProjectController struct {
	beego.Controller
}


func (c *ProjectController) GetProject() {
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	projectlist:=models.DataListProject()
	fmt.Println("proejct列表数据:")
	fmt.Println(projectlist)
	/*返回json数据*/

	//新增一条数据，并给模型赋值
	//user :=models.Create(1,"123")
	//fmt.Println(user)


	c.Data["datas"]= projectlist
	c.TplName = "project/list.html"
}

func (c *ProjectController) AddProject(){
	c.TplName = "project/project.html"
}

func (c *ProjectController) AddSaveProject(){
	name := c.Input().Get( "name")

	fmt.Printf("name=>>>>>>>>>>>>>>>>>", name)

	isOk, _:=models.CreateProject(name)
	fmt.Println(">>>>>>>>>>", isOk)
	if isOk == true {
		c.Redirect("/project/GetProject", 302)
	} else {
		c.Redirect("/project/AddProject", 302)
	}
}

func (c *ProjectController) DeleteProject(){
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	id := c.Input().Get( "id")
	_id,_:= strconv.Atoi(id)
	fmt.Println(_id)

	if _id == 0 {
		fmt.Println("参数错误");
	}

	_,err:=models.QueryByIdProject(_id)
	if err==true{
		fmt.Println("获取模型失败");
		fmt.Println(err);
	}

	ret := models.Del(_id);
	fmt.Printf("ret=%d", ret)

	c.Redirect("/project/GetProject", 302)
}

func (c *ProjectController) InfoProject() {

	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	id := c.Input().Get( "id")
	_id,_:= strconv.Atoi(id)
	fmt.Println(_id)
	if _id != 0 {
		userlist,err:=models.QueryByIdProject(_id)
		if err==true{
			fmt.Println("获取模型失败");
			fmt.Println(err);
		}
		fmt.Println(userlist)
		c.Data["data"]= userlist
	}
	c.TplName = "project/edit.html"
}



func (c *ProjectController) UpdateProject() {
	o := orm.NewOrm()
	o.Using("test")

	//获取参数
	_id := c.GetString( "id")
	id,_:= strconv.Atoi(_id)
	//fields["Id"]=id
	name:= c.GetString("name")

	fmt.Println("id >>>>>>>>>>>>>>>>>>>>", id);
	fmt.Println("name >>>>>>>>>>>>>>>>>>>>", name);

	project:=models.Project{
		Name:name,
	}
	//更新
	models.UpdateByIdProject(id, project)

	//路径的跳转
	c.Redirect("/test", 302)
}


func (c *ProjectController) GetListProject(){
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	projectList:=models.DataListProject();

	c.Data["json"] = projectList
	c.ServeJSON()
}
