package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"myproject/models"
	"fmt"
	"strconv"
)

type TestController struct {
	beego.Controller
}

func (c *TestController) Get() {
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	userlist:=models.DataList()
	fmt.Println("用户列表数据:")
	fmt.Println(userlist)
	/*返回json数据*/

	//新增一条数据，并给模型赋值
	//user :=models.Create(1,"123")
	//fmt.Println(user)


	c.Data["datas"]= userlist
	c.TplName = "test.html"
}


func (c *TestController) Add(){
	c.TplName = "add.html"
}

func (c *TestController) AddSave(){
	name := c.Input().Get( "name")

	fmt.Printf("name=>>>>>>>>>>>>>>>>>", name)

	isOk, _:=models.Create(name)
	fmt.Println(">>>>>>>>>>", isOk)
	if isOk == true {
		c.Redirect("/test", 302)
	} else {
		c.Redirect("/test/Add", 302)
	}
}

func (c *TestController) Delete(){
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	id := c.Input().Get( "id")
	_id,_:= strconv.Atoi(id)
	fmt.Println(_id)

	if _id == 0 {
		fmt.Println("参数错误");
	}

	_,err:=models.QueryById(_id)
	if err==true{
		fmt.Println("获取模型失败");
		fmt.Println(err);
	}

	ret := models.Del(_id);
	fmt.Printf("ret=%d", ret)

	c.Redirect("/test", 302)
}

func (c *TestController) Info() {

	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	id := c.Input().Get( "id")
	_id,_:= strconv.Atoi(id)
	fmt.Println(_id)
	if _id != 0 {
		userlist,err:=models.QueryById(_id)
		if err==true{
			fmt.Println("获取模型失败");
			fmt.Println(err);
		}
		fmt.Println(userlist)
		c.Data["data"]= userlist
	}
	c.TplName = "edit.html"
}



func (c *TestController) Update() {
	o := orm.NewOrm()
	o.Using("test")

	//获取参数
	_id := c.GetString( "id")
	id,_:= strconv.Atoi(_id)
	//fields["Id"]=id
	name:= c.GetString("name")

	fmt.Println("id >>>>>>>>>>>>>>>>>>>>", id);
	fmt.Println("name >>>>>>>>>>>>>>>>>>>>", name);

	user:=models.User{
		Name:name,
	}
	//更新
	models.UpdateById(id, user)

	//路径的跳转
	c.Redirect("/test", 302)
}


func (c *TestController) GetList(){
	o := orm.NewOrm()
	o.Using("test")
	//用户列表
	userlist:=models.DataList();

	c.Data["json"] = userlist
	c.ServeJSON()
}