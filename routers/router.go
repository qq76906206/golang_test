package routers

import (
	"myproject/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/test", &controllers.TestController{})
	beego.Router("/test/Info?:id", &controllers.TestController{}, "get:Info")
	beego.Router("/test/Add", &controllers.TestController{}, "get:Add")
	beego.Router("/test/AddSave", &controllers.TestController{}, "post:AddSave")
	beego.Router("/test/Delete?:id", &controllers.TestController{}, "get:Delete")
	beego.Router("/test/Update", &controllers.TestController{}, "post:Update")
	beego.Router("/test/GetList", &controllers.TestController{}, "get:GetList")
	beego.Router("/project/GetProject", &controllers.ProjectController{}, "get:GetProject")
	beego.Router("/project/InfoProject?:id", &controllers.ProjectController{}, "get:InfoProject")
	beego.Router("/project/AddProject", &controllers.ProjectController{}, "get:AddProject")
	beego.Router("/project/AddSaveProject", &controllers.ProjectController{}, "post:AddSaveProject")
	beego.Router("/project/DeleteProject?:id", &controllers.ProjectController{}, "get:DeleteProject")
	beego.Router("/project/UpdateProject", &controllers.ProjectController{}, "post:UpdateProject")
	beego.Router("/project/GetListProject", &controllers.ProjectController{}, "get:GetListProject")
}




